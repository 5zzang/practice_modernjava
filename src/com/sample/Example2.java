package com.sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * @author Taewoo, Kim(Michael) (5zzang@gmail.com)
 * @since 09-09-2015
 *
 * Functional Interface.
 */
public class Example2 {
    public static void main(String[] args) {
        /**
         * Function
         */
        // old style
//        Function<String, Integer> toInt = new Function<String, Integer>() {
//            @Override
//            public Integer apply(final String s) {
//                return Integer.parseInt(s);
//            }
//        };

        // lambda
        Function<String, Integer> toInt = s -> Integer.parseInt(s);

        final Integer number = toInt.apply("100");
        System.out.println(number);

        // Identity type
//        Function<Integer, Integer> identity = Function.identity();
        Function<Integer, Integer> identity = t -> t;
        System.out.println(identity.apply(999));


        /**
         * Consumer
         */
        // old style
//        final Consumer<String> print = new Consumer<String>() {
//            @Override
//            public void accept(final String s) {
//                System.out.println(s);
//            }
//        };

        // lambda
        final Consumer<String> print = s -> System.out.println(s);
        final Consumer<String> greeting = s -> System.out.println("Hello, " +s);
        print.accept("Hello!");
        greeting.accept("Michael");


        /**
         * Predicate
         */
        final List<Integer> numbers = Arrays.asList(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5);
        // old style
//        final Predicate<Integer> isPositive = new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                if (integer > 0) {
//                    return true;
//                }
//
//                return false;
//            }
//        };
        // lambda
        final Predicate<Integer> isPositive = i -> i > 0;
        final Predicate<Integer> lessThan3 = i -> i < 3;

        System.out.println("Positive numbers: " +filter(numbers, isPositive));
        System.out.println("Less than 3 numbers: " + filter(numbers, lessThan3));


        /**
         * Supplier
         */
        // old style
        final Supplier<String> hello = () -> "Hello ";
        System.out.println(hello.get() + "World!");

        long start = System.currentTimeMillis();
        printIfValidIndex(0, getVeryExpensiveValue());
        printIfValidIndex(-1, getVeryExpensiveValue());
        printIfValidIndex(-2, getVeryExpensiveValue());
        System.out.println("It took " + ((System.currentTimeMillis() - start) / 1000) + "seconds.");

        // Lazy Evaluation example
        start = System.currentTimeMillis();
        printIfValidIndex2(0, () -> getVeryExpensiveValue());
        printIfValidIndex2(-1, () -> getVeryExpensiveValue());
        printIfValidIndex2(-2, () -> getVeryExpensiveValue());
        System.out.println("It took " + ((System.currentTimeMillis() - start) / 1000) + "seconds.");
    }

    private static String getVeryExpensiveValue() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "Michael";
    }

    private static void printIfValidIndex(int number, String value) {
        if (number >= 0) {
            System.out.println("The value is " + value + ".");
        } else {
            System.out.println("Invalid");
        }
    }

    private static void printIfValidIndex2(int number, Supplier<String> supplier) {
        if (number >= 0) {
            System.out.println("The value is " + supplier.get() + ".");
        } else {
            System.out.println("Invalid");
        }
    }

    private static <T> List<T> filter(List<T> list, Predicate<T> filter) {
        List<T> result = new ArrayList<>();

        for (T input : list) {
            if (filter.test(input)) {
                result.add(input);
            }
        }

        return result;
    }
}
